<?php

namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;

use App\Models\Politica;
use App\Models\Event;
use App\Models\Aluno;
use App\Models\RelPoliticaProdutos;
use App\Models\Produtos;

use Input;
use DB;
use Image;
use Storage;
use Auth;

use App\Modules\Admin\Services\CustomHelpers;

use App\Modules\Admin\Requests;
use App\Modules\Admin\Controllers\Controller;


class ProdutosController extends Controller
{

    public $req;

    protected $view = "Admin::sections";

    public function __construct( Request $req )  
    {
        $this->req = $req;
    }

    public function init() 
    {

        $data = array();

        if( empty($this->req->input( "methods" )) ) {

            $auth = Auth::guard('admin_user')->user();

            $auth->foto = CustomHelpers::__get_before_photo_user($auth->id, $auth->foto);

            return view( $this->view .'.produtos', [
                "login" => 1,
                'user' => $auth
            ] );

        }else{
            if( $this->req->input( "methods" ) ) 
            {
                return call_user_func_array(array(
                    $this, 
                    $this->req->input("methods")
                ), array());
            }
        }
    }

    protected function show() { 

        $rel = array(); $ids = array();

        $product = Produtos::all();
        $data    = $product->toArray();

        foreach( $data as $i => $item ) {

            $rel[$i] = RelPoliticaProdutos::where("produto_id", "=", $item["id"])->get(array("politica_id"));
            $data[$i]["policys"] = $rel[$i]->toArray();

            $rel_delivery[$i] = DB::table('rel_servico_entrega')->where('produto_id', '=', $item['id'])->select('modulo_id')->get();
            $data[$i]["delivery"] = $rel_delivery[$i];

        }

        return $data;
    }

    protected function save() {
        
        $this->rulesValidate();

        $rules = $this->rulesQuery();
        
        //save produto
        $save = Produtos::create( $rules["query"] );
         
        $small = $this->req->input("post.small");

        if( isset($small) && count($small) != 0) {
            $this->__transport_file( $save->id, $small[0]['thumb'] );
        }

        
        //save policys
        if( isset($rules["policy"]) ) {

            foreach( $rules["policy"] as $i ) {

                RelPoliticaProdutos::create(array(
                    "produto_id"  => $save->id, 
                    "politica_id" => $i
                ));

            }

        }

        //save delivery
        if( isset($rules["delivery"]) ) {

            foreach( $rules['delivery'] as $i ) {

                $query_delivery[] = array(
                    'produto_id' => $save->id,
                    'modulo_id' => $i
                );

            }

            DB::table('rel_servico_entrega')->insert($query_delivery);

        }

        // criando diretorio
        $path = '/uploads/repositorio/';
        $dir  = $path . $save->id;
        // $dir  = $path . $save->id . str_slug($rules['query']['nome'], '-');
        
        if( !Storage::disk('local')->exists($dir) ) {
            Storage::makeDirectory($dir);
        }
        
        return array(
            "alert"=>array(
                "title"=>"Sucesso!",
                "text" =>"Produto \"<b>". $this->req->input("post.nome") ."</b>\" criada com sucesso.",
                "type" =>"success"
            ),
            "code"=>200
        );
    }

    protected function put() {

        $data; $rules; $query = array();

        $id   = $this->req->input("post.id");
        $post = $this->req->input("post");

        $this->rulesValidate();

        $rules = $this->rulesQuery();

        $getProduct = Produtos::find($id);
        $data       = $getProduct->toArray();

        unset($data['updated_at'], $data['created_at']);

        foreach( array_keys($rules['query']) as $i ) {


            if( $data[$i] != $rules["query"][$i] ) {
                $query[$i] = $rules["query"][$i];
            }

        }

        if( count($query) != 0 ) {

            // decimal
            if( isset($query['foto_unit_val']) && !empty($query['foto_unit_val']) ) {
                $query['foto_unit_val'] = $this->decimal( $query['foto_unit_val'] );
            }

            if( isset($query['regras']) && !empty($query['regras']) ) {
                
                $regras = json_decode($query['regras'], true);

                foreach($regras as $i => $item){
                    $regras[$i]['preco'] = $this->decimal( $item['preco'] );
                }

                $query['regras'] = json_encode($regras);
            }

            $_put_product = Produtos::where("id", "=", $id)->update( $query );

            if( isset($query['thumb_small']) ) {
                $this->__clean_thumb_small($id.'/thumb_small');
                $this->__transport_file( $id, $query['thumb_small'] );
            }

        }

        // relacionamento com produto
        $rels = array();
          
        if( isset($rules['policy']) ) {
            $rels['policy'] = $rules['policy'];
        }

        if( isset($rules['delivery']) ) {
            $rels['delivery'] = $rules['delivery'];
        }

        $_put_rels = $this->putRels( $id, $rels);
        
        if( $_put_rels == 1 || isset($_put_product) ) {
            return array(
                "alert"=>array(
                    "title"=>"Sucesso!",
                    "text" =>"Produto \"<b>". $post["nome"] ."</b>\" alterado com sucesso.",
                    "type" =>"success"
                )
            );
        }else{
            return array('status'=>0);
        }
    }

    protected function destroy() {

        $post = $this->req->input("post");
        $id   = $post["id"];

        $path = '/uploads/repositorio/';
        $dir  = $path . $id;
        // $dir  = $path . $id . str_slug($post['nome'], '-');

        if( Storage::disk('local')->exists($dir) ) {
            Storage::deleteDirectory($dir);
        }

        DB::table('fotos_produto')->where('produto_id', '=', $id)->delete();
        
        Produtos::where("id", "=", $id)->delete();
        RelPoliticaProdutos::where("produto_id", "=", $id)->delete();

        return array(
            "alert"=>array(
                "title" => "Sucesso!",
                "text"  => "Politica \"<b>". $post["nome"] ."</b>\" deletada com sucesso.",
                "type"  => "success"
            ),
            "code"=>200
        );
    }

    protected function putRels( $id, $arr ) 
    {

        $status = 0;

        extract($arr);

        if( isset($delivery) && count($delivery) != 0 ) {
            
            $get = DB::table('rel_servico_entrega')->where('produto_id', '=', $id)->get();

            if( count($get) != 0 ) {

                while(list($i, $item) = each($get)){
                    $verify[] = $item->modulo_id;
                }

                if(json_encode($delivery) != json_encode($verify)) {

                    foreach($delivery as $modulo) {
                        $data[] = array('produto_id' => $id,'modulo_id' => $modulo);
                    }

                    DB::table('rel_servico_entrega')->where('produto_id', '=', $id)->delete();
                    DB::table('rel_servico_entrega')->insert($data);

                    $status = 1;

                }

            }else{

                foreach($delivery as $modulo) {
                    $data[] = array('produto_id' => $id,'modulo_id' => $modulo);
                }

                DB::table('rel_servico_entrega')->insert($data);

                $status = 1;

            }


        }

        if( isset($policy) && count($policy) != 0 ) {

            unset($verify, $data);

            $get = DB::table('rel_politica_produto')->where('produto_id', '=', $id)->get();

            if( count($get) != 0 ) {


                while(list($i, $item) = each($get)){
                    $verify[] = $item->politica_id;
                }

                if(json_encode($policy) != json_encode($verify)) {

                    if( !empty($policy[0]) ) {

                        foreach($policy as $politica) {
                            $data[] = array('produto_id' => $id,'politica_id' => $politica);
                        }

                        DB::table('rel_politica_produto')->insert($data);

                    }

                    DB::table('rel_politica_produto')->where('produto_id', '=', $id)->delete();
                        

                    $status = 1;

                }

            }else{

                foreach($policy as $politica) {
                    $data[] = array('produto_id' => $id,'politica_id' => $politica);
                }

                DB::table('rel_politica_produto')->insert($data);

                $status = 1;

            } 

        }

        return $status;
    }

    protected function rulesQuery() 
    {

        $post = $this->req->input("post");
        $rules = array(); $policy = array(); $delivery = array();

        if( isset($post['small']) && is_array($post['small']) ) {

            foreach($post['small'] as $i => $item) {
                $post['small'] = $item['thumb'];
            }

        }else{ unset($post['small']); }
    
        if(isset( $post['thumb_small'] )) {
            unset($post['thumb_small']);
        }

        foreach( array_keys($post) as $i  ) {

            if($i == 'small') {
                $rules['thumb_small'] = $post['small'];
            }else{
                if( !is_array($post[$i]) ) {
                    $rules[$i] = $post[$i];
                }
            }

        }

        $rules['regras'] = json_encode( $post['regras'] );

        $data = array();

        $data['query'] = $rules;

        if( count($post['policys']) != 0 ) {
            $data['policy'] = $post['policys'];
        }

        if( count($post['delivery']) != 0 ) {
            $data['delivery'] = $post['delivery'];
        }
        
        return $data;
    }

    protected function rulesValidate() 
    {

        $rules = array(); 
        $fields = "event_id,nome,foto_unit_val";

        if( count( $this->req->input("post.delivery") ) == 0 && $this->req->input("post.instituicao_id") == 0 ) {
            $rules["post.delivery"] = "required";
        }

        // if( count( $this->req->input("post.policys") ) == 0 ) {
        //     $rules["post.policys"] = "required";
        // }

        foreach( explode(",", $fields) as $field) {
            $rules[ "post.". $field ] = "required";
        }

        $this->validate($this->req, $rules);
    }

    public function getPolicy() {

        return array(
            "data"=>Politica::get(array("id", "nome")),
            "code"=>200
        );
    }

    public function getAlunos() {

        return array(
            "data"=>Aluno::get(array("id", "nome")),
            "code"=>200
        );
    }

    public function getEvents() {

        return array(
            "data"=>Event::get(array("id", "name")),
            "code"=>200
        );
    }

    public function decimal( $valor ) 
    {
        return str_replace(',', '.', str_replace('.', '', $valor));
    }

    private function __get_type_service() 
    {
        $find = DB::table('admin_modulos')->where('tipo', '=', 'frete')->get();
        return $find;
    }

    public function __upload() 
    {

        $this->__clean_thumb_small('tmp');

        $file = Input::file('file');
        $path = '/uploads/repositorio/tmp';

        $tmp = $file->getRealPath();
        $name = md5(uniqid(rand(), true)) .".". $file->getClientOriginalExtension();

        $img = Image::make($tmp);

        $img->fit(128, 128, function ($constraint) {
            $constraint->upsize();
        });

        if( !Storage::disk('local')->exists( $path .'/'. $name ) ) {
            $img->save(public_path() . $path .'/'. $name);
        }

        return array(
            'data' => array(
                'local' => last(explode('/', $path)),
                'path' => $path,
                'thumb' => $name
            )
        );

    }

    public function __transport_file( $id, $foto ) {

        $directory = '/uploads/repositorio/tmp';
        $to = '/uploads/repositorio/'. $id .'/thumb_small';

        if( !Storage::disk('local')->exists( $to ) ) {
            Storage::makeDirectory($to);
        }

        foreach( Storage::allFiles($directory) as $file ) {
            Storage::move($file, $to .'/'. $foto);
        }

    }

    public function __clean_thumb_small( $local ) {

        $directory = '/uploads/repositorio/'. $local;

        foreach( Storage::allFiles($directory) as $file ) {

            if( Storage::disk('local')->exists( $file ) ) {
                Storage::delete($file);
            }

        }

    }

    public function __get_instituicoes() {

        return DB::table('instituicoes')->get();

    }


}