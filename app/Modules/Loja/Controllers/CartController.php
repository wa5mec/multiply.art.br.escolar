<?php

namespace App\Modules\Loja\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Loja\Helpers\EmailHelper;

// Facades
use Mail;
use Session;
use DB;
use URL;
use Auth;
use PagSeguro;

// models
use App\Models\Cart;
use App\Models\Pedido;
use App\Models\ItensPedido;
use App\Models\AbandonedCart;

// Helpers
use Carbon\Carbon;



class CartController extends Controller
{


	public $politicas = [];

	// adiciona imagens dentro da session
	public function addPhotosToCart(Request $request)
	{
		// declaração de variáveis
		$j = 0; $imagens = [];

		// verifica se existe variavel de confirmação
		// caso exista este request veio da tela de confirmação de fotos
		if( $request->input('confirmacao') === null ){

			//dd( Session::get('utils') );

			if(!Session::has('utils')){			

				$repositorios = DB::table('events')
				->where('events.id', '=', $request->input("produto_id"))
				->join('produtos as prod', 'events.id', '=', 'prod.event_id')
				->leftJoin('alunos', 'prod.aluno_id', '=', 'alunos.id')
				->select( 'events.parcelas', 'alunos.nome as nome_aluno', 'prod.nome as product_name', 'prod.id', 'prod.created_at as prod_data', 'prod.pin')
				->get();

				if( isset( $repositorios[0]->aluno_id)  ){
					$aluno_id = $repositorios[0]->aluno_id;
				}else{
					$aluno_id = 0;
				}
				 
				$utils = [
					'evento_id' => $repositorios[0]->event_id,
					'aluno_id' => $aluno_id,
					'produto_id' => $repositorios[0]->id,
					'parcelas_sem_juros' => $repositorios[0]->parcelas
				];

				$request->session()->put('utils', $utils );
			}

			// recupera instancia de carrinho, caso exista
			$oldCart = Session::has('cart') ? Session::get('cart') : null;			

			// recupera dados do cliente para adicionar a session
			$cliente = Auth::guard('cliente')->user();

			// recupera id do cliente logado
			$idUser = $cliente->id;

			// recupera array de imagens
			$reqImagens = $request->input('imagem');

			// busca todos os endereços do cliente logado
			$enderecos = DB::table('rel_conts_ends')
						->where([
							['rel_conts_ends.id_conta', '=', $idUser],
							['rel_conts_ends.tipo_de_conta', '=', 'cliente']
						])
						->join('enderecos as end', 'end.id', '=', 'rel_conts_ends.id_endereco')
						->select('end.*')
						->get();
			
			//  adiciona enderecos ao objeto clientes
			$cliente->enderecos = $enderecos;

			// verifica os ids selecionados pelo cliente
			// filtra as imagens selecionadas pelo cliente na tela anterior
			for($i = 0; $i < count($reqImagens); $i++){

				// caso exista o campo id no meu array, ele adiciona a minha lista de imagens em session
				if (array_key_exists('id',$reqImagens[$i])) {
					$imagens[$j] = $reqImagens[$i];
					$j++;
				}

			}// endfor



			if($j == 0){
				return redirect()->back()->withErrors(['message' => 'Escolha ao menos uma foto para prosseguir']);
			}

			// caso já existam imagens em session, zera os dadose retira os valores
			// código para corrigir erro de contagem duplicada das imagens, caso o usuário volte uma página via navegador
			if( isset($oldCart->imagens) ){
				
				// conta as imagens existentes em session
				$numImagens = count($oldCart->imagens);

				// multiplica numero de imagens pelo valor unitário
				$valTotImagens = $oldCart->precoUnidadeImagem * $numImagens;

				// retira o valor das imagens do preço total e atualiza o valor tiotal do pedido
				$oldCart->precoTotal = $oldCart->precoTotal - $valTotImagens;

				// zera objeto de imagens dentro da session
				$oldCart->imagens = [];
			}

			// cria instancia de carrinho
			$cart = new Cart( $oldCart );

			// adiciona dados de cliente ao carrinho
			$cart->addCliente( $cliente );

			// adiciona ao carrinho todas imagens escolhidas pelo cliente
			$cart->addImagens( $imagens );

			// just return true
			$cart->genTotalValueImages( $request->input('produto_id') );

			// insere na sessão o objeto de Carrinho Cart::all()
			$request->session()->put( 'cart', $cart );

			// recebe a página originadora do request
			$pagAnterior = URL::previous();

			// get Session Object
			$session = $request->session();

			// salva a sessão no banco de dados
			$this->saveCartSession( $session, $idUser, $pagAnterior );

			$clean = array();

			// recupera url das imagens para passar para a view
			for($i = 0; $i < count($cart->imagens); $i++){
				$clean[$i] = DB::table('fotos_produto')->where('id', '=', $cart->imagens[$i]['id'])->select('url')->first();
				$request->session()->get('cart')->imagens[$i]['url'] = $clean[$i]->url;
			}

			// transforma retorno em objeto
			$galeria = json_decode( json_encode($request->session()->get('cart')->imagens, false) );

			return view("Loja::loggedin.galeria-confirm",compact('galeria'));


		}else{

			$reqImagens = $request->input("imagem");

			for($i = 0; $i < count($reqImagens); $i++){

				// caso exista o campo id no meu array, ele adiciona a minha lista de imagens em session
				if (array_key_exists('id',$reqImagens[$i])) {
					$imagens[$j] = $reqImagens[$i];
					$j++;
				}

			}// endfor

			$numOldImages = count($request->session()->get('cart')->imagens);
			$numAtualImages = count($imagens);

			// caso o usuário tenha excluido alguma imagem, atualiza valor com novas imagens
			if( $numAtualImages != $numOldImages ){
				
				$imgenUnidade = $request->session()->get('cart')->precoUnidadeImagem;
				
				$precoTotal = $request->session()->get('cart')->precoTotal;

				$maxImagesDesconto = $request->session()->get('cart')->maxImagesDesconto;

				$precoAtualizado = ($precoTotal - ($numOldImages * $imgenUnidade)) + ($numAtualImages * $imgenUnidade);

				if( $maxImagesDesconto < $numAtualImages){
					$valDesconto = ($maxImagesDesconto * $imgenUnidade);
				}else{
					$valDesconto = ($numAtualImages * $imgenUnidade);
				}				

				if( $precoAtualizado < 0 ){
					$precoAtualizado = 0.00;
				}

				$request->session()->get('cart')->valDesconto = $valDesconto;
				$request->session()->get('cart')->precoTotal = $precoAtualizado;
			}

			// adiciona novas images asession
			$request->session()->get('cart')->imagens = $imagens;

			return redirect("opcoes/" . $request->session()->get('utils')['produto_id'] );

			// gerencia pagina de escolha de impresso ou digital
			//$util = json_decode(json_encode($request->session()->get('utils')), false);		
			//return view("Loja::loggedin.tipo-produto", compact("util"));
			
		}

	} // end addPhotosToCart()


	// método para persistencia dos dados
	public function saveCartSession($session, $idUser, $url = null)
	{
		// seleciona Session
		$cartSession = AbandonedCart::where([
			['cliente_id', '=', $idUser],
			['status', '=', 1]
		])->get();

		// caso sessão exista, atualiza os dados de carrinho abandonado
		if( count($cartSession) > 0 ){
			$cart = AbandonedCart::find( $cartSession[0]->id );

			// faz a atualização dos dados
			$cart->update([
				'session' => json_encode($session->get('cart')),
				'url' => $url
			]);

		}else{ // caso não exista, cria uma nova

			// cria uma nova linha no banco de dados
			$create = AbandonedCart::create([
				'session' => json_encode( $session->get('cart') ),
				'cliente_id' => $idUser
			]);
		}

		return true;

	} //end saveCartSession()


	// método gerenciador de politicas
	public function addOptionsToCart(Request $request)
	{
		$politicas = $request->input('politicas');
		$radios = $request->input('radio');
		$politicasCart = [];
		$counter = 0;

		for( $i = 0; $i < count($politicas); $i++ ){

			if( array_has( $politicas[$i], 'option' ) ){
				
				foreach($politicas[$i] as $key => $value){
					$politicasCart[$counter][$key] = $value;
				}

				$counter++;

			}else{

				if( count($radios) > 0 ){
					foreach($radios as $radio ){
						
						if($radio == $i ){

							foreach($politicas[$i] as $key => $value){
								$politicasCart[$counter][$key] = $value;
							}

							$counter++;
						}
					}
				}
			}
		}

		// recupera instancia de carrinho, caso exista dentro da Session
		$oldCart = Session::has('cart') ? Session::get('cart') : null;
		
		// caso o usuário volte um passo via navegador, retira a soma inseria antes de ele voltar o passo
		if( count($oldCart->politicas) > 0 ){
			
			$valorPoliticas = 0;
			
			for($i = 0; $i < count($oldCart->politicas); $i++ ){
				$valorPoliticas += $oldCart->politicas[$i]['preco'];
			}
			$oldCart->precoTotal = ($oldCart->precoTotal - $valorPoliticas);
		}

		// cria instancia de carrinho
		$cart = new Cart( $oldCart );

		// dd($cart);

		// adiciona ao carrinho todas politicas escolhidas pelo cliente
		$cart->addPoliticas( $politicasCart );

		// cria instancia de carrinho dentro da session
		$request->session()->put( 'cart', $cart );

		// recupera id do cliente para inserção na session
		$idUser = Auth::guard('cliente')->user()->id;

		// recebe a página originadora do request
		$pagAnterior = URL::previous();

		// get Session Object
		$session = $request->session();

		// salva a sessão no banco de dados
		$this->saveCartSession( $session, $idUser, $pagAnterior );

		return redirect()->route("frete.index");
	}


	// adiciona frete ao carrinho de compra
	public function addFreteToCart(Request $request)
	{
		$this->validate($request, [
			'prazo' => 'required',
			'valor' => 'required',
			'cep' => 'required',
			'endereco_id' => 'required',
			'metodo_frete' => 'required'
		]);

		// valida forma correta de valor
		if( $request->input("valor") == 0){
			$valor = 0.00;
		}else{
			$valor = $request->input("valor");
		}

		$frete = [
			'metodo_frete' =>  $request->input("metodo_frete"),
			'prazo' => $request->input("prazo"),
			'valor' => $valor,
			'cep' => $request->input("cep"),
			'endereco_id' => $request->input("endereco_id")
		];
		$request->session()->get("cart")->frete = $frete;
		$cart = $request->session()->get("cart");
		$utils = json_decode(json_encode($request->session()->get("utils"), false));

		// verifica se o pedido é grátis
		if($cart->precoTotal == 0){
			return view("Loja::loggedin.finaliza-compra-gratis", compact("cart","utils"));
		}		

		return view("Loja::loggedin.finaliza-compra", compact("cart","utils"));
	}

	

	/* adiciona frete e dados de pagamento ao carrinho de compra
	*	Este método é somente para compras que usam o tipo DIGTAL
	*	este método é pois o TIPO DIGITAL parte do processo de compra
	*/
	public function addFreteAndPaymentDataToCart(Request $request)
	{
		$this->validate($request, [
			'prazo' => 'required',
			'valor' => 'required',
			'cep' => 'required',
			'endereco_id' => 'required'
		]);

		$frete = [
			'prazo' => $request->input("prazo"),
			'valor' => $request->input("valor"),
			'cep' => $request->input("cep"),
			'endereco_id' => $request->input("endereco_id")
		];

		$request->session()->get("cart")->frete = $frete;
		$cart = $request->session()->get("cart");
		$utils = json_decode(json_encode($request->session()->get("utils")), TRUE);

		return view("Loja::loggedin.finaliza-compra", compact("cart","utils"));		
	}



	public function buildPayment(Request $request)
	{
		$cart = $request->session()->get("cart");

		$this->validate( $request,
			[
				'name' => 'required',
				'last_name' => 'required',
				'email' => 'required|email',
				'data_nascimento' => 'required',
				'celular' => 'required',
				'cpf' => 'required'
			],
			[
				'name.required' => 'O campo Nome é obrigatório!',
				'last_name.required' => 'O campo Sobrenome é obrigatório!',
				'email.required' => 'O campo Email é obrigatório!',
				'email.email' => 'Digite um Email válido!',
				'data_nascimento.required' => 'O campo Data de Nascimento é obrigatório!',
				'celular.required' => 'O campo Data de Nascimento é obrigatório!',
				'cpf.required' => 'O campo Data de Nascimento é obrigatório!'
			]
		);


		$utils = json_decode(json_encode($request->session()->get("utils")), true);
		
		$desconto = ( -1 * (str_replace( ',', '.', $cart->valDesconto) ) );

		$itens = $endereco = [];
		$contador = 0;

		// insere imagens dentro de itens
		for($i = 0; $i < count($cart->imagens); $i++){			

			$itens[$i] = [
				'itemId' => $cart->imagens[$i]['id'],
				'itemDescription' => 'Foto',
				'itemAmount' => str_replace( ',', '.', $cart->precoUnidadeImagem),
				'itemQuantity' => '1',

			];
		}

		$contador = 0;

		// contador de politicas
		$contador = count($itens);

		// insere politicas dentro de itens
		for($j = 0; $j < count($cart->politicas); $j++){
			$itens[$contador] = [
				'itemId' => $cart->politicas[$j]['politica_id'],
				'itemDescription' => $cart->politicas[$j]['titulo'],
				'itemAmount' => str_replace( ',', '.', $cart->politicas[$j]['preco'] ),
				'itemQuantity' => '1'
			];
			$contador++;
		}

		// insere endereço escolhido pelo cliente
		for($i = 0; $i < count($cart->cliente->enderecos); $i++){
			
			// procura endereço nos endereços cadastrados pelo cliente
			if( $cart->frete['endereco_id'] == $cart->cliente->enderecos[$i]->id){

				$endereco['shippingAddressStreet'] = $cart->cliente->enderecos[$i]->logradouro;
				$endereco['shippingAddressNumber'] = $cart->cliente->enderecos[$i]->numero;
				$endereco['shippingAddressDistrict'] = $cart->cliente->enderecos[$i]->bairro;
				$endereco['shippingAddressPostalCode'] = $cart->cliente->enderecos[$i]->cep;
				$endereco['shippingAddressCity'] = $cart->cliente->enderecos[$i]->cidade;
				$endereco['shippingAddressState'] = 'SP';
				
			}else{ // caso não encontre o endereço escolhido na lista de cliente
				// recupera dados de endereço da instituição(estilo de retirada na escola)
				$enderecoInstituicao = DB::table('enderecos')->find($cart->frete['endereco_id']);
				
				// verifica se endereço da instituição realmente existe no banco de dados
				if( count($enderecoInstituicao) > 0){
					// seta dados de endereço da instituição para o pag seguro
					$endereco['shippingAddressStreet'] = $enderecoInstituicao->logradouro;
					$endereco['shippingAddressNumber'] =  $enderecoInstituicao->numero;
					$endereco['shippingAddressDistrict'] = $enderecoInstituicao->bairro;
					$endereco['shippingAddressPostalCode'] = $enderecoInstituicao->cep;
					$endereco['shippingAddressCity'] = $enderecoInstituicao->cidade;
					$endereco['shippingAddressState'] = $enderecoInstituicao->estado;
				}

				
			}

		}

		$idPedido = $cart->cliente->id.date('dmy').rand(1000, 9999);

		try {
    		//chamar os métodos da bilbioteca aqui dentro

			$telefone = (strlen(trim($cart->cliente->telefone)) < 10 ) ? $cart->cliente->telefone : $cart->cliente->celular;

			$pagseguro = PagSeguro::setReference($idPedido)
				->setSenderInfo([
					'senderName' =>  $request->input('name') .' '.  $request->input('last_name'), //Deve conter nome e sobrenome
					'senderPhone' => $telefone, //Código de área enviado junto com o telefone
					'senderEmail' =>  $request->input('email'),
					'senderHash' => $request->input('senderHash'),
					'senderCPF' =>  $request->input('cpf') //Ou CNPJ se for Pessoa Júridica
				])
				->setCreditCardHolder([
					'creditCardHolderName' =>  $cart->cliente->name .' '. $cart->cliente->last_name, //Deve conter nome e sobrenome
					'creditCardHolderPhone' => $telefone, //Código de área enviado junto com o telefone
					'creditCardHolderCPF' => $cart->cliente->cpf, //Ou CNPJ se for Pessoa Júridica
					'creditCardHolderBirthDate' => $cart->cliente->data_nascimento,
				])
				->setShippingAddress($endereco)
				->setBillingAddress($endereco)
				->setItems(
					$itens
				)
				->setShippingInfo([
					'shippingType' => '1', //: 1 – PAC, 2 – SEDEX, 3 - Desconhecido
					'shippingCost' => str_replace( ',', '.', $cart->frete['valor'] )
				])
				->setExtraAmount( $desconto )
				->send([
					'paymentMethod' => 'creditCard',
					'creditCardToken' => $request->input('token'),
					'installmentQuantity' => ( $request->input('parcelas') > 0 ) ? $request->input('parcelas') : 1,
					'installmentValue' => (str_replace( ',', '.', $request->input('valor_parcela')) == 0.00) ? str_replace( ',', '.', $request->input('valorTotal')) : str_replace( ',', '.', $request->input('valor_parcela')),
					'noInterestInstallmentQuantity' => $utils['parcelas_sem_juros']
				]);
		}
		catch(\Artistas\PagSeguro\PagSeguroException $e) {
			
			$error =[ 
				'error' => [
					'code' => $e->getCode(), 
					'message' => $e->getMessage()
				]
			];

			return json_encode($error,false);
		}

		$paymentData = json_encode($pagseguro);
		// $retornoPagSeguro = json_decode($paymentData, false)

		// invoca construtor de pedidos
		$this->buildOrder($idPedido, json_decode($paymentData, false), $request, $itens);

		// deleta carts da session
		$request->session()->forget('cart');

		// deleta utils da session
		$request->session()->forget('utils');

		// retorna todos os dados da compra
		return $paymentData;
	}


	/*
	*	constroi o pedido
	*	recebe os dados do método de pagamento
	*/
	protected function buildOrder($numPedido, $payment, $request, $itens = 0)
	{

		$cart = $request->session()->get("cart");
		$utils = json_decode(json_encode($request->session()->get("utils")), TRUE);		

		$pedido = Pedido::create([
			'num_pedido' => $numPedido,
			'endereco_id' => $cart->frete['endereco_id'],
			'code_transacao' => $payment->code,
			'valor_frete'	=> $cart->frete['valor'],
			'prazo_entrega' => $cart->frete['prazo'],
			'metodo_frete' => $cart->frete['metodo_frete'],
			'referencia' =>  $payment->reference,
			'cliente_id' => $cart->cliente->id,
			'valor_pedido' => $payment->grossAmount,
			'status' => $payment->status,
			'total_parcial' => $payment->netAmount,
			'desconto' => $cart->valDesconto,
			'tipo_pagamento' => $payment->type
		]);


		for($i = 0; $i < count($cart->imagens);$i++){
			$item = [
				'pedido_id' => $pedido->id,
				'produto_id' => $utils['produto_id'],
				'item_id' => $cart->imagens[$i]['id'],
				'quantidade' => 1,
				'valor_unitario' => $cart->precoUnidadeImagem,
				'descricao' => $cart->imagens[$i]['mensagem'],
				'titulo' => 'Foto'
			];
			ItensPedido::create($item);
		}

		for($i = 0; $i < count($cart->politicas);$i++){
			$item = [
				'pedido_id' => $pedido->id,
				'produto_id' => $utils['produto_id'],
				'item_id' => $cart->politicas[$i]['politica_id'],
				'quantidade' => 1,
				'valor_unitario' =>$cart->politicas[$i]['preco'],
				'descricao' => 'politica',
				'titulo' => 'Opcionais: '. $cart->politicas[$i]['titulo']
			];
			ItensPedido::create($item);
		}

		// insere todos os retornos do pagseguro para validação posterior
		DB::table('transaction_pagseguro')->insert([
			'pedido_id' => $pedido->id, 
			'pedido_code' => $numPedido,
			'json' => json_encode($payment)
		]);

		// recupera dados para disparar emails de confirmação de compra
		// e nova ordem de compra
		$pedidos = $this->getOrderDataTo( $pedido->id );
		$this->sendClientNewOrderEmail($pedidos);
		$this->sendAdminNewOrderEmail($pedidos);

		return true;

	}


	public function buildFreePayment(Request $request)
	{
		$cart = $request->session()->get("cart");
		$utils = json_decode(json_encode($request->session()->get("utils")), TRUE);	
		$idPedido = $cart->cliente->id . date('dmy') . rand(1000, 9999);

		$payment = [			
			'code' => $idPedido.'0000',
			'reference' => $idPedido,
			'netAmount' => 0.00,
			'status' => 3,
			'type' => 0,
			'grossAmount' => $cart->valDesconto,
		];

		$payment =  json_decode(json_encode( $payment ), false);
		
		$this->buildOrder( $idPedido, $payment, $request, 0 );

		// deleta carts da session
		$request->session()->forget('cart');
		
		// deleta utils da session
		$request->session()->forget('utils');

		return json_encode( $payment );
	}


	public function getOrderDataTo($id)
	{
		$pedidos = DB::table('pedidos as p')
					->where('p.id', $id)
					->leftJoin('itens_pedidos as ip', 'ip.pedido_id', '=', 'p.id')
					->leftJoin('enderecos as end', 'end.id', '=', 'p.endereco_id')
					->leftJoin('produtos as prod', 'prod.id', '=', 'ip.produto_id')
					->leftJoin('events', 'events.id', '=', 'prod.event_id')
					->leftJoin('clientes as cli','cli.id','=','p.cliente_id')
					->leftJoin('fotos_produto as fotos', 'fotos.id', '=', 'ip.item_id')
					->select(						
						'prod.nome as prod_nome',
						'events.name as event_name', 
						'end.*', 
						'ip.*', 
						'p.*',
						'cli.name as nome_cliente',
						'cli.last_name as sobrenome_cliente',
						'cli.email as email',
						'fotos.name as src'
					)->get();

		return $pedidos;
	}


	public function sendClientNewOrderEmail($pedidos)
	{		
		// $to = 'atendimento@multiply.art.br';
		// $to = 'neferson.oliveira@wa5.com.br';
		$to = $pedidos[0]->email;
		$email = new EmailHelper();
		$email->confirmaCompraCliente( $to, $pedidos );
		return $pedidos;
	}


	public function sendAdminNewOrderEmail($pedidos)
	{
		$to = 'atendimento@multiply.art.br';
		// $to = 'neferson.oliveira@wa5.com.br';
		$email = new EmailHelper();
		$email->setAdminNewOrderEmail( $to, $pedidos );
		return $pedidos;
	}


	public function killSessionCart(Request $request)
	{
		$request->session()->forget("cart");
		return json_encode(array('success' => true));
	}

}
