<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{

    protected $table = 'produtos';

    protected $fillable = [
        "event_id",
        "aluno_id",
        "instituicao_id",
        "nome",
        'thumb_small',
        "pin",
        "foto_unit_val",
        "regras",
        "status",
        "descricao"
    ];

}
