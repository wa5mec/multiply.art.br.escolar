<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelPoliticaProdutos extends Model
{

    protected $table = 'rel_politica_produto';

    protected $fillable = [
        "produto_id",
        "politica_id"
    ];


}
